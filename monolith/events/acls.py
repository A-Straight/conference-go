from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search"
    my_param = {'query': f'{city}, {state}'}
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=my_param)
    data = json.loads(response.text)
    photo = data['photos'][0]['src']['original']
    return photo

def get_weather_data(city, state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid={OPEN_WEATHER_API_KEY}"
    geo_response = requests.get(geo_url)
    geo_data = json.loads(geo_response.text)
    print(geo_data)
    lat = geo_data[0]['lat']
    lon = geo_data[0]['lon']
    # location = {'lattitude': lat, 'longitude': lon}
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_data = json.loads(weather_response.text)
    temp = weather_data['main']['temp']
    description = weather_data['weather'][0]['description']
    results = {'Temperature': temp, 'Description': description}
    return results
